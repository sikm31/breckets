package org.bdshadow.interview;

import org.apache.commons.lang3.StringUtils;

public class Brackets {

    public boolean isCorrect(String s) {
        int a  = StringUtils.countMatches(s, "(");
        int b  = StringUtils.countMatches(s, ")");
        return a == b;
    }
}
